import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from "@angular/forms";
import { DataService } from "./service/data.service";
import { Node, NodeInfo } from "./interfaces/interfaces";
import {ValueConverter} from "@angular/compiler/src/render3/view/template";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  nameControl: FormControl = new FormControl();
  distanceControl: FormControl = new FormControl();
  fileData: any;
  showOutput = false;
  showErrorMessage = false;
  noOutput = false;
  setOfMn: Node[];
  setOfMd: NodeInfo[];

  constructor(
    private dataService: DataService
  ) {}

  ngOnInit () {
    this.nameControl.setValidators([Validators.required]);
    this.distanceControl.setValidators([Validators.required, Validators.pattern('[0-9]*(.)?[0-9]*')]);
  }

  onReceiveData(fileData) {
    if (fileData) {
     this.fileData = fileData;
    } else {
      this.fileData = null;
    }
  }

  onClick() {
    if ((!this.fileData) || (!this.nameControl.value) || (!this.distanceControl.value)) {
      this.setOfMn = [];
      this.setOfMd = [];
      this.showErrorMessage = true;
      this.showOutput = false;
      setTimeout(() => {
        this.showErrorMessage = false;
      }, 3000);
      return;
    }

    this.showErrorMessage = false;
    this.showOutput = true;
    const name = this.nameControl.value;
    const distance = this.distanceControl.value;
    this.setOfMn = this.getValidNodeNames(name);
    this.setOfMd = this.getInDistanceNodes(this.setOfMn, distance);
    if (this.setOfMn.length !== 0) {
      this.noOutput = false;
    } else {
      this.noOutput = true;
    }
  }

  private getValidNodeNames(name: string) {
    if (!this.fileData) {
      return [];
    } else {
      return this.dataService.getNodesNameIncludeField(this.fileData, name);
    }
  }

  private getInDistanceNodes(nodes: Node[], distance: string) {
    if (!this.fileData) {
      return [];
    } else {
      return this.dataService.getInDistanceNodes(this.fileData, nodes, parseFloat(distance));
    }
  }

}
