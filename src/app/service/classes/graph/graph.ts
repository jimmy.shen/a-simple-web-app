import { PriorityQueue } from "../priority-queue/priority-queue";
import { Node } from "../../../interfaces/interfaces";

export class Graph {
  edges = {};
  nodes: Node[] = [];

  private edgeExist(node1: string, node2: string) {
    for (const edge of this.edges[node1]) {
      if (edge.node === node2) {
        return true;
      }
    }
    return false;
  }

  private getNodeObject(name: string) {
    for (const node of this.nodes) {
      if (node.name === name) {
        return node;
      }
    }
    return null;
  }

  addNode(node: Node) {
    this.nodes.push(node);
    this.edges[node.name] = [];
  }

  addEdge(node1: string, node2: string, weight: number = 1) {
    if (!this.edgeExist(node1, node2)) {
      this.edges[node1].push({ node: node2, weight: weight });
    }
  }

  addWeight() {
    for (const node of this.nodes) {
      for (const data of this.edges[node.name]) {
        const node2 = this.getNodeObject(data.node);
        data.weight = Math.sqrt((node.x - node2.x) ** 2 + (node.y - node2.y) ** 2);
      }
    }
  }

  display() {
    let graph = "";
    this.nodes.forEach(node => {
      graph += node.name + "->" + this.edges[node.name].map(n => {
        return "("+ n.node + ", " + n.weight + ")"
      }).join(", ") + "\n";
    });
    console.log(graph);
  }

  djikstraAlgorithm(startNode: Node) {
    let distances = {};

    // Stores the reference to previous nodes
    let prev = {};
    const pq = new PriorityQueue();

    // Set distances to all nodes to be infinite except startNode
    distances[startNode.name] = 0;
    pq.enqueue(startNode.name, 0);

    this.nodes.forEach(node => {
      if (node.name !== startNode.name) distances[node.name] = Infinity;
      prev[node.name] = null;
    });

    while (!pq.isEmpty()) {
      const minNode = pq.dequeue();
      const currNode = minNode.element;
      const weight = minNode.priority;
      this.edges[currNode].forEach(neighbor => {
        let alt = distances[currNode] + neighbor.weight;
        if (alt < distances[neighbor.node]) {
          distances[neighbor.node] = alt;
          prev[neighbor.node] = currNode;
          pq.enqueue(neighbor.node, distances[neighbor.node]);
        }
      });
    }
    return distances;
  }

  getMinDistanceOfNodes(nodes: Node[], limit: number) {
    const validNodes = {}; // {name: {node: nodeObj, distance: distance}}
    nodes.map(node => {
      const distances = this.djikstraAlgorithm(node);
      for (let [key, value] of Object.entries(distances)) {
        //const keyNode = this.getNodeObject(key);
        if (value < limit) {
          if (validNodes[key]) {
            validNodes[key].distance = validNodes[key].distance > value ? value : validNodes[key].distance;
          } else {
            const keyNode = this.getNodeObject(key);
            validNodes[key] = {node: keyNode, distance: value};
          }
        }
      }
    });
    return validNodes;
  }

}
