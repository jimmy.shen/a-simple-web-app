import { Injectable } from '@angular/core';
import { Graph } from "./classes/graph/graph";
import { Node } from "../interfaces/interfaces";

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor() { }

  constructGraph(data: any) {
    const graph = new Graph();
    const jsonData = JSON.parse(data);
    jsonData.map(node => {
      graph.addNode(node);
      node.connections.map(neighbour => {
        graph.addEdge(node.name, neighbour);
      })
    })
    graph.addWeight();
    return graph;
  }

  getNodesNameIncludeField(data: any, name: string) {
     const jsonData = JSON.parse(data);
     return jsonData.filter(node => node.name.includes(name));
  }

  getInDistanceNodes(data: any, nodes: Node[], distance: number) {
    const graph = this.constructGraph(data);
    //graph.display();
    const resultObj = graph.getMinDistanceOfNodes(nodes, distance);
    const nodesInfo = [];
    for (let [key, value] of Object.entries(resultObj)) {
      nodesInfo.push(value);
    }
    return nodesInfo;
  }

}
