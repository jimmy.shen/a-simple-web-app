import { Component, ElementRef, EventEmitter, Input, Output, OnInit, ViewChild } from '@angular/core';
import { FileUploadInfo, FileUpload } from "../interfaces/interfaces";

@Component({
  selector: 'app-file-uploader',
  templateUrl: './file-uploader.component.html',
  styleUrls: ['./file-uploader.component.css']
})
export class FileUploaderComponent implements OnInit {
  @Input() fileSizeLimitInMB = 10;
  @Input() showProgress = true;
  @Input() acceptFileTypes = ['.txt', '.json'];
  @Output() uploadedFile = new EventEmitter();
  @ViewChild('uploadInput', { read: ElementRef }) uploadInput: ElementRef;

  buttonText = 'Choose File';
  touched = false;
  fileUploadInfo: FileUploadInfo;

  constructor() { }

  ngOnInit(): void {
  }

  private initialFileUploadInfo() {
    this.fileUploadInfo = {
      status: 'none'
    } as FileUploadInfo;
    if (this.uploadInput) {
      this.uploadInput.nativeElement.value = '';
    }
  }

  onClickChoose(ev) {
    this.uploadInput.nativeElement.click();
  }

  onChoosedFile(ev) {
    const files = ev.target.files;
    if (!files || files.length === 0) {
      return;
    }

    this.fileUploadInfo = {
      fileUpload: null,
      status: 'ready',
      failedMessage: null
    };

    this.touched = true;

    if (files.length > 1) {
      this.fileUploadInfo.status = 'not-one';
      return;
    }

    const file = files[0];
    if (file.size > this.fileSizeLimitInMB * 1000000) {
      this.fileUploadInfo.status = 'oversize';
      return;
    }

    const fileUpload = {
      name: file.name,
      data: '',
      size: file.size
    } as FileUpload;

    this.fileUploadInfo.fileUpload = fileUpload;

    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      fileUpload.data = fileReader.result;
      this.fileUploadInfo.fileUpload = fileUpload;
      this.fileUploadInfo.status = 'completed';
      this.uploadedFile.emit(this.fileUploadInfo.fileUpload.data);
    }
    fileReader.readAsText(file);

  }

  onClickDeleteOrCancel(ev) {
    this.initialFileUploadInfo();
    this.uploadedFile.emit(null);
  }

  getFileSizeText(size: number) {
    if (size >= 1000 * 1000 * 1000) {
      return Math.round(size / 1000 / 1000 / 10) / 100 + ' GB';
    } else if (size >= 1000 * 1000) {
      return Math.round(size / 1000 / 10) / 100 + ' MB';
    } else if (size >= 1000) {
      return Math.round(size / 10) / 100 + ' MB';
    } else {
      return size + ' Bytes';
    }
  }
}
