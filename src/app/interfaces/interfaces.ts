export interface FileUpload {
  name: string;
  data: any;
  size: number;
}

export interface FileUploadInfo {
  fileUpload: FileUpload;
  status:
    | 'none'
    | 'not-one'
    | 'oversize'
    | 'ready'
    | 'started'
    | 'paused'
    | 'success'
    | 'completed'
    | 'cancelled'
    | 'failed';
  failedMessage?: string;
}

export interface Node {
  name: string;
  x: number;
  y: number;
  connections: string[];
}

export interface NodeInfo {
  node: Node;
  distance: number;
}

